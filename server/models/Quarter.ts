/**
This file identifies the required schema for Mongoose operations to the quarters collection in MongoDB.
*/

var mongoose = require('mongoose');

var QuarterSchema = new mongoose.Schema({
    name: String,
	displayName: String,
    startTime: Date,
    endTime: Date
});

//Locates Mongo DB entries using supplied quarter. Returns only building, building key, and comfort values. Used specifically to load the map stats information. Excludes entries that are incomplete.
QuarterSchema.statics.findQuarterMenu = function findQuarterMenu(cb) {
	let day = 24*60*60*1000;												//Milliseconds in a day
	let timeBuffer = new Date(+new Date - (14*day));						//Automatic update at 2 weeks past the quarter start
    return this.find({
		startTime: {
            $lte: timeBuffer
        }
    }).sort({
        startTime: -1
    }).select('name displayName').exec(cb);
};

module.exports = mongoose.model('Quarter', QuarterSchema);
