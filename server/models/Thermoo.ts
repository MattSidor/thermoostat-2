/**
This file identifies the required schema for Mongoose operations to the thermoos collection in MongoDB.
In addition, the following static operations with custom API queries are established:
 - findByBuilding
 - findByBuildingKey
 - findByDate
 - findByDateRange
 - findByQuarter
 - findByQuarterBuilding
 - findByRoom
 - findBySession
 - findByUser
*/

var mongoose = require('mongoose');

var ThermooSchema = new mongoose.Schema({
    _id: Number,
    username: String,
    ucdPersonUuid: Number,
    firstName: String,
    lastName: String,
    emailAddress: String,
    isFaculty: Boolean,
    isStaff: Boolean,
    isStudent: Boolean,
    casSession: String,
    sessionClosed: Boolean,
    quarter: String,
    timestamp: {
        type: Date,
        default: Date.now
    },
    building: String,
    buildingKey: String,
    room: String,
    notCurrentlyHere: Boolean,
    previouslyHereDate: String,
    previouslyHereTime: String,
    comfort: String,
	clothing: String,
    activity: String,
    comment: String,
    satisfaction: String,
    started: String,
    utm_campaign: String,
    utm_source: String,
    emailConfirmation: Boolean,
	isComplete: Boolean
});


//Custom API Functions

//Locates Mongo DB entries using supplied building name.
ThermooSchema.statics.findByBuilding = function findByBuilding(name, cb) {
    return this.where('building', name).exec(cb);
};

//Locates Mongo DB entries using supplied building key.
ThermooSchema.statics.findByBuildingKey = function findByBuildingKey(key, cb) {
    return this.where('buildingKey', key).exec(cb);
};

//Locates Mongo DB entries using supplied date to presemt.
ThermooSchema.statics.findByDate = function findByDate(dateStart, cb) {
    var gteDate = (new Date(dateStart)).toISOString();
    return this.find({
        timestamp: {
            $gte: gteDate
        }
    }, cb);
};

//Locates Mongo DB entries using supplied dates from start to end.
ThermooSchema.statics.findByDateRange = function findByDate(dateStart, dateEnd, cb) {
    var gteDate = (new Date(dateStart)).toISOString();
    var lteDate = (new Date(dateEnd)).toISOString();
    return this.find({
        timestamp: {
            $gte: gteDate,
            $lte: lteDate
        }
    }, cb);
};

//Locates Mongo DB entries that are older than hour and have not been set to complete by the AirTable function.
ThermooSchema.statics.findByIncomplete = function findByIncomplete(cb) {
    var lteDate = new Date();
    lteDate.setHours(lteDate.getHours()-1);
    var finalDate = lteDate.toISOString();
    return this.find({
        timestamp: {
            $lte: finalDate
        },
        sessionClosed: false
    }, cb);
};

//Locates Mongo DB entries using supplied quarter sorted by Building Key in reverse order.
ThermooSchema.statics.findByQuarter = function findByQuarter(qrtr, cb) {
    return this.where('quarter', qrtr).sort({
        buildingKey: -1
    }).exec(cb);
};

//Locates Mongo DB entries using supplied quarter. Returns only building, building key, and comfort values. Used specifically to load the map stats information. Excludes entries that are incomplete.
ThermooSchema.statics.findByQuarterSum = function findByQuarterSum(qrtr, cb) {
    return this.find({
        quarter: qrtr,																	
        buildingKey: {																				//Remove any incomplete data entries.
            $nin: ['', null]
        },
        building: {
            $nin: ['', null]
        },
        comfort: {
            $nin: ['', null]
        }
    }).sort({
        buildingKey: 1
    }).select('building buildingKey comfort').exec(cb);
};

//Locates Mongo DB entries using supplied quarter and building name. Used for final building summary.
ThermooSchema.statics.findByQuarterBuilding = function findByQuarterBuilding(data, cb) {
    return this.where('quarter', data[0]).where('building', data[1]).exec(cb);
};

//Locates Mongo DB entries using supplied quarter and building. Returns only building, building key, and comfort values. Used specifically to load results on the summary page. Excludes entries that are incomplete.
ThermooSchema.statics.findByQuarterBuildingSum = function findByQuarterBuildingSum(data, cb) {
    return this.find({
		quarter: data[0],																	
		building: data[1],
        comfort: {
            $nin: ['', null]
        }
    }).select('building buildingKey comfort').exec(cb);
};


//Locates Mongo DB entries using supplied building name and room.
ThermooSchema.statics.findByRoom = function findByRoom(data, cb) {
    console.log(data[0] + data[1]);
    return this.where('building', data[0]).where('room', data[1]).exec(cb);
};

//Locates Mongo DB entries using supplied casSession key. Returns only the _id to server side functions for the process of posting data. Before lookup, key is scrubbed for potential spoofed keys or Mongo DB wildcard functions.
ThermooSchema.statics.findBySession = function findBySession(session, cb) {
    var safeSession = session.replace(/[^a-zA-Z0-9!-_]/g, '');
    var timeCheck = new Date();
    timeCheck.setHours(timeCheck.getHours() - 1);
    return this.find({
        casSession: safeSession,
        sessionClosed: false,
        timestamp: {
            $gte: timeCheck
        }
    }).sort({timestamp: -1}).select('_id').limit(1).exec(cb);
};

//Locates Mongo DB entries using supplied UCD username.
ThermooSchema.statics.findByUser = function findByUser(loginId, cb) {
    return this.where('username', loginId).exec(cb);
};

module.exports = mongoose.model('Thermoo', ThermooSchema);