/**
This file identifies the required schema for Mongoose operations to the apikeys collection in MongoDB.
In addition, the following static operations with custom API query is established:
 - verify
*/

var mongoose = require('mongoose');

var ApiSchema = new mongoose.Schema({
    apiKey: String,
    userName: String,
    active: Boolean
});

ApiSchema.statics.verify = function verify(key, cb) {
    /* Prevent MongoDB injection attacks by limiting API keys to alphanumeric without special characters. */
    var safeKey = key.replace(/[^a-zA-Z0-9]/g, '');
    return this.findOne({
        'apiKey': safeKey
    }, 'active').exec(cb);
};

module.exports = mongoose.model('ApiKey', ApiSchema);