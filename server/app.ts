/** 
Setup server side application with API, connections to MongoDB, helmet protection, and partial record to AirTable handling.
*/

import * as express from 'express';
import { json, urlencoded } from 'body-parser';
import * as path from 'path';
import * as cors from 'cors';
import * as compression from 'compression';

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var intervals = require('./interval');
var helmet = require('helmet');

import { facLinkRouter } from "./routes/facLink";
import { envConfig } from './config';
var thermoos = require('./routes/thermoo');
var quarter = require('./routes/quarter');
var map = require('./routes/map');
var cas = require('./routes/cas');
var feedback = require('./routes/feedback');
var summary = require('./routes/summary');

// load mongoose package
var mongoose = require('mongoose');

// Use native Node promises
mongoose.Promise = global.Promise;

// connect to MongoDB
mongoose.connect(envConfig[process.env.NODE_ENV].mongoConfig)
    .then(() => console.log('API to Database connection succesful.'))
    .catch((err) => console.error(err));

const app: express.Application = express();

// //Helmet settings definition for additional security.
// app.use(helmet.contentSecurityPolicy({
//     directives: {
//         defaultSrc: ["'none'"],
//         scriptSrc: ["'self'", "'unsafe-inline'",'*.google-analytics.com'],
//         styleSrc: ["'self'", "'unsafe-inline'"],
//         imgSrc: ["'self'", "data:",'*.google-analytics.com'],
//         connectSrc: ["'self'"],
//         fontSrc: ["'self'", "data:"]
//     },
// }));
// app.use(helmet.xssFilter());  //Cross-site Scripting vulnerability prevention.
// app.use(helmet.hidePoweredBy({ setTo: 'Joules-Ware' }));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.disable('x-powered-by');

app.use(json());
app.use(compression());
app.use(urlencoded({ extended: true }));

// api routes
app.use('/api/faclink', facLinkRouter);
app.use('/api/thermoo', thermoos);
app.use('/api/quarter', quarter);
app.use('/api/map', map);
app.use('/api/cas', cas);
app.use('/api/feedback', feedback);
app.use('/api/summary', summary);

app.use(express.static(path.join(__dirname, '/../client')));

//'catch-all' route middleware to capture routes with query params
app.use(function(req, res) {
    res.sendFile(path.join(__dirname, '/../client/index.html'));
});

// catch 404 and forward to error handler
app.use(function(req: express.Request, res: express.Response, next) {
    let err = new Error('Not Found');
    next(err);
});

// production error handler
// no stacktrace leaked to user
app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {

    res.status(err.status || 500);
    res.json({
        error: {},
        message: err.message
    });
});

var expireSessions = function(){setInterval(function(){intervals.closeExpired();}, 5*60*1000);} //Run every 5 minutes. Expires sessions after an hour has passed from initial record creation. Relevant to CAS only.
//Initial launch on server start
expireSessions();

export { app }