/** 
Primary function to parse through incomplete records that are older than hour past intial creation. If the partial record made it past the comfort page, it will be passed to AirTable and then marked as closed. If not, it will be marked for session closed without any additional steps required. Closed session records can no longer be edited by users through the app.
*/

var Thermoo = require('./models/Thermoo.js');
var airtable = require('./airtable/airtable');

/* Called by the app.ts in 5 minute intervals from server start. */
export function closeExpired() {
    var cursor = Thermoo.findByIncomplete().cursor();
    cursor.on('data', function (doc) {
        if(doc.comfort!=null)                           // If incomplete record made it past comfort page, pass to AirTable for processing.
            airtable.sendToAT(doc);
        else{                                           // else close the session.
            Thermoo.update({
                _id: doc.id
            }, {
                $set: {
                    sessionClosed: true
                }
            }).exec();
        }
    });
};