/** 
Primary functions for sending closed session data to Airtable. This specifically acts as middleware to connects and transform data into the format required by the AirTable in use. The airtable that it connects to is based on the variables defined in the config.ts file under airTableConfig. After the data is successfully transferred to AirTable and a confirmation response is received, the database entry is updated to set the record sessionClosed entry to true. This allows for AirTable maintenance/service outages to not impact TherMOOstat behavior.
*/

let Thermoo = require('../models/Thermoo.js');
import { envConfig } from '../config';
let config = envConfig[process.env.NODE_ENV].airTableConfig;

let Airtable = require('airtable');
let base = new Airtable({apiKey : config.apiKey}).base(config.apiBase);

/* Send data to AirTable */
export let sendToAT = function(data){
    let atData = structureAT(data);
    base('Feedback').create(atData, {typecast: true}, function(err, res) {          //Create a new line entry in AirTable.
        if(err) { console.error(err); return;}
        if(res.id.indexOf('rec')===0 && !data.sessionClosed){                       //Confirmation response from AirTable will start with rec.
            let body = {"sessionClosed" : true};
            Thermoo.findByIdAndUpdate(data._id, body, function (err, post) {        //Update entry in MongoDB.
                if (err) console.log(err);
            });
        }
    });
};


/*  Middleware to convert the app to Airtable consumable  */
let structureAT = function(data) {
    let build = {
        "Date" : data.timestamp,
        "Building Name" : data.building,
        "Room" : data.room,
        "Alternate Date" : data.previouslyHereDate + " " + data.previouslyHereTime,
        "Comfort" : capLetter(data.comfort),
        "Physical Activity" : capLetter(data.activity),
        "Comment" : data.comment,
        "Email Confirmation" : data.emailConfirmation,
        "First Name" : data.firstName,
        "Email" : data.emailAddress,
        "Change in Last..." : capLetter(data.started),
		"Clothing Level" : capLetter(data.clothing)
    };
    let cleanBuild = stripEmpty(build);                                             //Remove optional fields to create a clean data package.
    return cleanBuild;
};

/* Converts Activity input to the format that matches AirTable - Deprecated per version 2.1 */
let activityTranslate = function(act) {
    let activity = "";                              //Default condition for no selection
    if(act != ""){
        activity = capLetter(act) + " Activity";    //Adds Activity for most condiitions.
        if(act === "none")
            activity = "No Activity";               //Converts for No Activity case.
    }
    return activity;
}

/* Capitalizes the first letter for the string passed in. */
let capLetter = function(item) {
    let re = /(\b[a-z](?!\s))/g;  //Regex to capitalize first letter for certain cases.
    if(item!=""){
        let capped = item.replace(re, function(x){return x.toUpperCase();})
        return capped;
    }
    return item;
}

/* Remove optional fields if they are left blank by the user. Required to prevent AirTable from refusing the API data.  */
let stripEmpty = function(data) {
    if (data["Physical Activity"] === "")
        delete data["Physical Activity"];           //Removes the empty entry from the JSON object.
    if (data["Change in Last..."] === "")
        delete data["Change in Last..."];           //Removes the empty entry from the JSON object.
    if (data["Clothing Level"] === "")
        delete data["Clothing Level"];           //Removes the empty entry from the JSON object.
    return data;
}