/**
 Primary functions for sending feedback to the database during a user session in the app.
 */

var express = require('express');
var router = express.Router();
var session = require('express-session');
var airtable = require('../airtable/airtable');

import { envConfig } from '../config';

const rp = require('request-promise');

//Set the config information for quick access.
let db = envConfig[process.env.NODE_ENV].dbConfig;

//Data is sent to '/api/post/casID' from the feedback service. Body should include the information to be updated. Note that a post action is performed. This middleware converts it to a PUT that goes into an existing record in Mongo DB.
router.post('/post/:id', (req, res) => {
    if(req.params.id === "null"){												//Check for null to allow for Guest sessions to continue. Returns sucess without any additional action if Guest.
        res.json("Success");
        return;
    }
    let getOptions = {                                             				//Set parameters for GET to MongoDB API.
        method: 'GET',
        uri: db.apiUri + db.apiKey + '/session/'+req.params.id,					//Use casSession to retrieve the Mongo DB _id that will be updated.
        json: true
    };
    rp(getOptions)                                                 				//Request promise
        .then(function (dbData) {
            if(dbData[0]===undefined)											//If no session is matched or the session has expired, data returned will be undefined.
                setTimeout(function() {res.json("Session error");}, 2000);		//Add a 2 second delay before handling invalid session in order to reduce chances for Denial of Service attacks.
            else{
                let putOptions = {                                             //Set parameters for POST to MongoDB API.
                    method: 'PUT',
                    uri: db.apiUri + db.apiKey + '/' + dbData[0]._id,
                    body: req.body,
                    json: true
                };
                rp(putOptions)                                            	 	//Request promise
                    .then(function (dbRes) {
                        if(req.body.isComplete){								//Verify if the record isComplete. This is set on the final submission at the comments page. 
                            let atOptions = {                                   //Set parameters for GET to MongoDB API.
                                method: 'GET',
                                uri: db.apiUri + db.apiKey + '/' + dbData[0]._id,
                                json: true
                            };
                            rp(atOptions)
                                .then(function(fullData){						//If it is complete, gather the full record and post to AirTable.
                                    if(fullData.comfort!=null)					//Verify the record does have comfort feedback.
                                        airtable.sendToAT(fullData);})
                                .catch(function (err) {console.log(err);});
                        }
                        res.json("Success");									//Send success on completion whether AirTable was included or not.
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            }
        })
        .catch(function (err) {
            console.log(err);
        });
});

module.exports = router;