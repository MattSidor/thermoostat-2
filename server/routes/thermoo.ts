/**
Primary functions for performing CRUD operations on the thermoos collection in MongoDB.
The standard format is: serveraddress/thermoo/apikey/parameters
All functions require an active API key to be entered as the first of the request parameters.

The following GET operations are available:
 - All data: /thermoo/yourAPIkey/
 - Specific record: /thermoo/yourAPIkey/_id
 - Building records: /thermoo/yourAPIkey/building/buildingName
 - Room records: /thermoo/yourAPIkey/building/buildingName/RoomName
 - Date records: /thermoo/yourAPIkey/date/startRange
 - Date range records: /thermoo/yourAPIkey/date/startRange/endRange
 - Quarter records: /thermoo/yourAPIkey/quarter/quarterName
 - Quarter Building records: /thermoo/yourAPIkey/quarter/quarterName/buildingName
 - User records: /thermoo/yourAPIkey/user/userName

The following POST operation is available:
 - New record: /thermoo/yourAPIkey/
 
The following PUT operation is available:
 - Update record: /thermoo/yourAPIkey/_id
 */

var express = require('express');
var router = express.Router();

var Thermoo = require('../models/Thermoo.js');
var Quarter = require('../models/Quarter.js');
var Apikey = require('../models/apiKey.js');

/* Identify if a key is valid */
var processKey = function (payload) {
    if (payload === null)
        return false;
    return payload.active;
}

/* Return an error message if the API key is invalid. */
var errorMessage = function () {
    console.log("Invalid API key.");
    return ([{
        Error: "Your API key is invalid. Please contact a system administrator if this is an error."
            }]);
}

/*Tabulates the comfort sums for the building and JSON data passed in. Also, identifies a Building Key if one exists among the records. */
var comfortSum = function (building, dataCopy) {
    var total = dataCopy.length,
		cold = 0,
        chilly = 0,
        perfect = 0,
        warm = 0,
        hot = 0,
        buildingKey = "";
    for (let i = 0; i < dataCopy.length; i++) {
		if(dataCopy[i].comfort===null){
			total--;
			continue;
		}
        let lowercaseComfort = dataCopy[i].comfort.toLowerCase();
        if (lowercaseComfort === "cold")
            cold++;
        else if (lowercaseComfort === "chilly")
            chilly++;
        else if (lowercaseComfort === "perfect")
            perfect++;
        else if (lowercaseComfort === "warm")
            warm++;
        else if (lowercaseComfort === "hot")
            hot++;
        if(dataCopy[i].buildingKey)
            buildingKey = dataCopy[i].buildingKey;
    }
    return {																		//Returned JSON object of data compiled.
        'buildingName' : building,
        'buildingKey' : buildingKey,
		'total': total,
        'comfortSummary': {
            'cold': cold,
            'chilly': chilly,
            'perfect': perfect,
            'warm': warm,
            'hot': hot
        }
    };

}

/* Function used in the user summary. Determines the number of times a person has added a comment with their feedback. */
var commentAdded = function (data) {
    var total = 0;
    for (var i = 0; i < data.length; i++)
        if(data[i].comment!=null && data[i].comment !=="")
            total++;
    return total;
}

/* GET /thermoo/:api listing. Returns all feedback form the database. Could take a while to complete. */
router.get('/:api', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.find(function (err, thermoo) {
            if (err) return next(err);
            res.json(thermoo);
        });
    });
});

/* POST /thermoo/:api Creates a new record in the database. */
router.post('/:api', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        req.body.quarter = "Quarter data unavailable";
        var compare = new Date();
        var start = [];
        var end = [];
        var academicYearDates = [];
        Quarter.find({}, function (err, post) {											//Identify the current quarter based on timestamp and set it to the record automatically.
            if (err) return next(err);
            academicYearDates = post;
            for (var i = 0; i < academicYearDates.length; i++) {
                start[i] = new Date(academicYearDates[i].startTime);
                end[i] = new Date(academicYearDates[i].endTime);
                if (start[i] <= compare && compare <= end[i])							//Check if the date is in range of quarter.
                    req.body.quarter = academicYearDates[i].name;
            }
            var identify = Thermoo.findOne({}, '_id', {									//Get the next available _id in the database and set as _id for new entry.
                sort: {
                    _id: -1
                }
            }, function (err, post2) {													
                var new_id = parseInt(post2._id) + 1;
                req.body._id = new_id;
                Thermoo.create(req.body, function (err, post3) {						//Create the new record.
                    if (err) return next(err);
                    res.json(post3);													//Return the response of the new record entered.
                });
            });
        });
    });
});

/* GET /thermoo/:api/id Get the specified record information. */
router.get('/:api/:id', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findById(req.params.id, function (err, post) {							//Returns a single record with the specified _id.
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/building/:building Returns all the data from a specific building */
router.get('/:api/building/:building', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByBuilding(req.params.building, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/summary/building/:building Returns a summary of the building provided */
router.get('/:api/summary/building/:building', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByBuilding(req.params.building, function (err, post) {
            if (err) return next(err);
            res.json(comfortSum(req.params.building, post));            				//If data does exist, parse it through the comfortSum function above to create a summary object to return.
        });
    });
});

/* GET /thermoo/:api/date/:start Returns the data from the date specified to current */
router.get('/:api/date/:start', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByDate(req.params.start, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/date/:start/:end Returns the data from the date range specified */
router.get('/:api/date/:start/:end', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByDateRange(req.params.start, req.params.end, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/key/:buildingKey Returns all data for the specified Building Key */
router.get('/:api/key/:buildingKey', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByBuildingKey(req.params.buildingKey, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/summary/key/:buildingKey Returns a summary of the data for the Building Key provided */
router.get('/:api/summary/key/:buildingKey', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByBuildingKey(req.params.buildingKey, function (err, post) {
            if (err) return next(err);
            res.json(comfortSum(post[0].building, post));    								//If data does exist, parse it through the comfortSum function above to create a summary object to return.        
        });
    });
});

/* GET /thermoo/:api/quarter/:quarter/:building Returns the quarter data for a specified building */
router.get('/:api/quarter/:quarter/:building', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        var parm = [req.params.quarter, req.params.building];
        Thermoo.findByQuarterBuilding(parm, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/summary/quarter/:quarter/:building Returns the summary data for the specified building within that quarter. */
router.get('/:api/summary/quarter/:quarter/:building', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        var parm = [req.params.quarter, req.params.building];

        Thermoo.findByQuarterBuildingSum(parm, function (err, post) {
            if (err) return next(err);
            res.json(comfortSum(parm[1], post));
        });
    });
});

/* GET /thermoo/:api/building/:building/:room Returns the data for the specified building and room combination. */
router.get('/:api/building/:building/:room', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        var parm = [req.params.building, req.params.room];
        Thermoo.findByRoom(parm, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/quarter/:quarter Returns all the data from the specified quarter. */
router.get('/:api/quarter/:quarter', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByQuarter(req.params.quarter, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/session/:key Gets the specified record based on the session key provided. */
router.get('/:api/session/:key', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findBySession(req.params.key, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/summary/quarter/:quarter Returns the summary data for all buildings for the specified quarter. */
router.get('/:api/summary/quarter/:quarter', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByQuarterSum(req.params.quarter, function (err, post) {
            if (err) return next(err);
            var buildingArray = [post[0]],													//Initial array of data received is sorted by buildingKey
                finalData = [];																//Empty array that will receive data summary per building as it's compiled
            for (var i = 1; i < post.length; i++) {											//Start with second record for comparison
                var prev = i - 1;															//Previous for comparison
                if (post[i].buildingKey != post[prev].buildingKey) {
                    finalData.push(comfortSum(post[prev].building, buildingArray));			//Determine the end of a set of building data and push that summary onto the stack
                    buildingArray = [];
                }
                buildingArray.push(post[i]);
                if (i === (post.length - 1))												//Check if on final record
                	finalData.push(comfortSum(post[i].building, buildingArray));			//Push final data
            }
            res.json(finalData);
        });
    });
});


/* GET /thermoo/:api/user/:username Returns all feedback for the specified user. Must be the UC Davis Login ID */
router.get('/:api/user/:username', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByUser(req.params.username, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* GET /thermoo/:api/summary/user/:username Returns the summary stats for the specified user. Must be the UC Davis Login ID */
router.get('/:api/summary/user/:username', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Thermoo.findByUser(req.params.username, function (err, post) {							//Gather all records for the specified user
            if (err) return next(err);
            if(post[0] === null){
                res.json(post);
                return;
            }
            let stats = {};
            if(post[0] != null)																	//If records exist, build out the stats object
                stats = {
                    name: post[0].firstName +" "+ post[0].lastName,								//First and last name
                    totalUse: post.length,														//Number of times logged into the app through CAS
                    totalComments: commentAdded(post),											//Number of times added comments to a record
                    feedback: comfortSum("votes", post)											//Number of votes submitted. May differ from use since comfort is later
                };
            res.json(stats);																	//Return the stats object after building
        });
    });
});

/* PUT /thermoo/:api/:id Updates an existing record Primary function called within the app during a user session */
router.put('/:api/:id', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        var options = {
            upsert: false																		//Do not allow entries that have not been created through the CAS process
        };
        Thermoo.findByIdAndUpdate(req.params.id, req.body, options, function (err, post) {		//Push the request body into the record to update
            if (err) return next(err);
            res.json(post);
        });
    });
});

module.exports = router;