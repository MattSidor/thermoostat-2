/**
Middleware functions for getting public data in the quarter collection in MongoDB.
The standard format is: serveraddress/api/map/
No API key is required as the data is publicly available on the UC Davis website.

The following GET operation is available:
 - Quarter map data: /map/quarter/
 - Weather forecast: /map/temperature/
*/

var express = require('express');
var router = express.Router();
const rp = require('request-promise');

var Quarter = require('../models/Quarter.js');

import { envConfig } from '../config';

//Set the config information for quick access.
let pi = envConfig[process.env.NODE_ENV].piTemperature;
let gov = envConfig[process.env.NODE_ENV].govWeather;
let token = envConfig[process.env.NODE_ENV].govToken;

let weather = {
	temperature : null,
	condition : null
};
let cacheDate = new Date();

let parseGov = function(data) {
	return data.slice(data.indexOf("textDescription")+19,data.indexOf('\",\n        \"icon'));
};

/* GET /map/quarter/ Used to automatically update the map quarter selection */
router.get('/quarter', function (req, res, next) {
	Quarter.findQuarterMenu(function (err, post) {
		if (err) return next(err);
		res.json(post);
	});
});

router.get('/temperature', function(req, res){
	let timestamp = new Date();
	if(cacheDate < timestamp || weather.temperature==null){
		rp({
			uri : pi,
			json : true
			}).then((data) => {
					if(data.Value !== null)
						weather.temperature = Math.round(data.Value);
/**				rp({
					uri : gov,
					headers : {
						'Accept':'application/x-www-form-urlencoded',
						'Content-Type': 'application/json',
						'User-Agent' : token
					}
				}).then((govData) => {
					if(parseGov(govData) !== "null")
						weather.condition = parseGov(govData);
					res.json(weather);
				}).catch((err) => {
					console.log(err);
					res.render('error');
				});*/
				res.json(weather);
				cacheDate.setMinutes(timestamp.getMinutes() +5);			//reset the cache to 5 minutes in the future.
				return null;
			}).catch((err) => {
				console.log(err);
				res.render('error');
			});
	}
	else{
		res.json(weather);													//Return cached data
	}
});


module.exports = router;