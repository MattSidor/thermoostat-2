/**
 Methods for getting the summary data from the api. Acts as middleware with limited access. Does not require key verification on the client side.
 */

var express = require('express');
var router = express.Router();
var session = require('express-session');

import { envConfig } from '../config';

const rp = require('request-promise');

//Set the config information for quick access.
let db = envConfig[process.env.NODE_ENV].dbConfig;

//Returns building feedback for the specified building.
router.get('/building/:name', (req, res) => {
    let getOptions = {                                             //Set parameters for GET to MongoDB API.
        method: 'GET',
        uri: db.apiUri + db.apiKey + '/summary/building/'+req.params.name,
        json: true
    };
    rp(getOptions)                                                 //Request promise
        .then((dbData) =>res.json(dbData))
        .catch((err)=>res.json("Building summary data not available."));
});

//Returns building feedback for all buildings for the selected quarter.
router.get('/quarter/:qrtr', (req, res) => {
    let getOptions = {                                             //Set parameters for GET to MongoDB API.
        method: 'GET',
        uri: db.apiUri + db.apiKey + '/summary/quarter/'+req.params.qrtr,
        json: true
    };
    rp(getOptions)                                                 //Request promise
        .then((dbData) =>res.json(dbData))
        .catch((err)=>res.json("Quarter summary data not available."));
});

//Returns building feedback for all buildings for the selected quarter.
router.get('/quarter/:qrtr/:bldg', (req, res) => {
    let getOptions = {                                             //Set parameters for GET to MongoDB API.
        method: 'GET',
        uri: db.apiUri + db.apiKey + '/summary/quarter/'+req.params.qrtr+'/'+req.params.bldg,
        json: true
    };
    rp(getOptions)                                                 //Request promise
        .then((dbData) =>res.json(dbData))
        .catch((err)=>res.json("Quarter Building summary data not available."));
});


module.exports = router;