/** 
Primary functions for receiving Facilities Link building data for the application.
*/

let env = process.env.NODE_ENV.toString();
import { Router, Response, Request } from 'express';
import { envConfig } from '../config';

const rp = require('request-promise');

const facLinkRouter: Router = Router();

let facLinkDataCache = null;
let previousDateParsed = null;
let config = envConfig[process.env.NODE_ENV].facLinkConfig;

facLinkRouter.get('/allBuildings', (request: Request, response: Response) => {
    let todaysDate = new Date();
    todaysDate.setHours(0,0,0,0);  //set to midnight
    let currentDateParsed = todaysDate.toJSON().slice(0,10);

    if(!facLinkDataCache || previousDateParsed !== currentDateParsed) {						//Check for cached data or if cache is current to today, continue with request if not
        rp({
            uri: config.apiUri,
            qs: {
                apiKey: config.apiKey														//Adds the api key to the query string as a parameter
            },
            json: true
        }).then((data) => {
            facLinkDataCache = data;														//Set the received data into the cache variable for future queries
            response.json(facLinkDataCache);												//Return the new data
            previousDateParsed = currentDateParsed;											//Update the timestamp for the cache
        }).catch((err) => {
            console.log(err);
            response.render('error');
        });
    }else {
        response.json(facLinkDataCache);													//Cached data exists, return the cache
    }

});

export { facLinkRouter }
