/**
Primary functions for performing CRUD operations on the quarter collection in MongoDB.
The standard format is: serveraddress/quarter/apikey/parameters
All functions require an active API key to be entered as the first of the request parameters.

The following GET operation is available:
 - All data: /quarter/yourAPIkey/

The following POST operation is available:
 - New record: /quarter/yourAPIkey/
 
*/

var express = require('express');
var router = express.Router();

var Quarter = require('../models/Quarter.js');
var Apikey = require('../models/apiKey.js');

/* Identify if a key is valid */
var processKey = function (payload) {
    if (payload === null)
        return false;
    return payload.active;
}

/* Return an error message if the API key is invalid. */
var errorMessage = function () {
    console.log("Invalid API key.");
    return ([{
        Error: "Your API key is invalid. Please contact a system administrator if this is an error."
            }]);
}


/* GET /quarter/:api */
router.get('/:api', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Quarter.find(function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

/* POST /quarter/:api */
router.post('/:api', function (req, res, next) {
    Apikey.verify(req.params.api, function (err, result) {
        if (!processKey(result)) {
            res.json(errorMessage());
            return;
        }
        Quarter.create(req.body, function (err, post) {
            if (err) return next(err);
            res.json(post);
        });
    });
});

module.exports = router;