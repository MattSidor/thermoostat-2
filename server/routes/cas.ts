/**
 Primary functions for connecting to the Central Authentication Service at UC Davis.

 The function gets the data from CAS after a user has signed in. From the login ID, it connects to the UCD Identity Access Management (IAM) system to obtain contact, and user information.
 After full information has been received, it POSTs this data to a MongoDB entry.
 */

var express = require('express');
var router = express.Router();
var session = require('express-session');
var CASAuthentication = require('cas-authentication');

import { envConfig } from '../config';
const rp = require('request-promise');

//Set the config information for quick access.
let cas = envConfig[process.env.NODE_ENV].apiCasConfig,
    iam = envConfig[process.env.NODE_ENV].apiIamConfig,
    db = envConfig[process.env.NODE_ENV].dbConfig;

//Establish a JSON object for gathering information to POST later.
let person = {
    username: "",
    ucdPersonUuid: 0,
    firstName: "",
    lastName: "",
    emailAddress: "",
    isFaculty: false,
    isStaff: false,
    isStudent: false,
    casSession: "",
    sessionClosed: false
};

//Generates a random 10 character string to create a unique session ID for data reference.
let charBuild = function() {
    let ans = "";
    let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!-_";
    for(var i=0; i<10; i++)
        ans += chars.charAt(Math.floor(Math.random() * chars.length));
    return ans;
};

//Initialize a CAS session.
router.use( session({
    secret: cas.secret,
    resave: cas.resave,
    saveUninitialized : cas.saveUninit
}));

//Set CAS paramters.
let casAuth = new CASAuthentication({
    cas_url: cas.apiUri,
    service_url : cas.base
});

//User information gathering process.
router.get( '/', casAuth.bounce, function ( req, res ) {
    let iamId = {
        uri: iam.apiUri,
        qs: {                                                   		//Query String paramters for IAM API.
            userId: req.session.cas_user,                       		//Gets the login ID of the user after authentication through CAS.
            key: iam.apiKey,
            v: iam.version
        }
    };
    let rngID = charBuild();           									//Append random 10 character string to build session.
    person.casSession = req.sessionID + rngID;
    rp(iamId)                                                   		//Request promise
        .then( (casData) => {
            let data = JSON.parse(casData);
            person.username = data.responseData.results[0].userId;  	//Store login ID.
            let iamId = data.responseData.results[0].iamId;         	//Store IAM ID. Used for next query to IAM.
            let iamPerson = {                                      	 	//Parameters for gathering user information.
                    uri: iam.userUri+iamId,
                    qs: {
                        key: iam.apiKey,
                        v: iam.version
                    },
                    json: true
                },
                iamContact = {                                          //Paramters for gathering email address.
                    uri: iam.contactUri+iamId,
                    qs: {
                        key: iam.apiKey,
                        v: iam.version
                    },
                    json: true
                };
            rp(iamPerson)                                           	//Request promise
                .then((iamData) => {                                    //Set collected information to person.
                    person.firstName = iamData.responseData.results[0].dFirstName;
                    person.lastName = iamData.responseData.results[0].dLastName;
                    person.ucdPersonUuid = iamData.responseData.results[0].iamId;
                    person.isFaculty = iamData.responseData.results[0].isFaculty;
                    person.isStaff = iamData.responseData.results[0].isStaff;
                    person.isStudent = iamData.responseData.results[0].isStudent;
                    rp(iamContact)                                      //Request promise
                        .then( (personData) => {                        //Set collected email to person.
                            person.emailAddress = personData.responseData.results[0].email;
                            let options = {                             //Set parameters for POST to MongoDB API.
                                method: 'POST',
                                uri: db.apiUri + db.apiKey,
                                body: person,
                                json: true
                            };
                            rp(options)                                 //Request promise
                                .then(function (dbData) {
                                    res.redirect( '/location?session=' + dbData.casSession );      //Redirect to next step once completed.
                                })
                                .catch(function (err) {
                                    console.log(err);
                                });
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                })
                .catch((err) => {
                    console.log(err);
                });
        })
        .catch((err) => {
            console.log(err);
        });
});

//Build and return the appropriate hyperlink for the welcome page based on the the node environment variable.
router.get('/envLink', (req,res) => {
    res.json(cas.apiUri + "/login?service=" + cas.base + "/");
});

//Rerouting if CAS redirects improperly.
router.get('/undefined', (req, res) => {
    res.redirect('/api/cas/');
});

module.exports = router;