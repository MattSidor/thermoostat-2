![thermoostat.png](https://bitbucket.org/repo/kpdLjk/images/1112074048-thermoostat.png)

# TherMOOstat 2.0 #
## Installation/Development Configuration ##

1. Clone Project 
   git clone git@bitbucket.org:MattSidor/thermoostat-2.git

2. NPM 
   Run in terminal to download necessary packages 
   npm install

3. Set-up environment variable 

   If running on TEST MongoDB 
   a) export NODE_ENV=test (Mac) or set NODE_ENV=dev (Windows) 
   b) Must be connected to the campus network in order to connect 
   c) Use npm start to begin the program 

   If running on DEV(LOCAL) MongoDB 
   a) export NODE_ENV=dev (Mac) or set NODE_ENV=local (Windows) 
   b) Install MongoDB to your local machine and run over the default port 27017 
   c) Run mongod in Terminal before starting. 
   d) Use npm start to begin the program

## Production Configuration ##

To Check If Running: 
1. forever list

To Stop: 
1. forever stop

To Start: 
1. cd fmeco-full-production/thermoostat-2 
2. forever start

## Credits ##

### Designer ###

*Jessica Blizard*

### Developers ###

*Matt Sidor*

*
Mike Loranc*

### Project Managers ###

*
Kiernan Salmon*

### Affiliations at University of California, Davis ###


*Facilities Management: Energy Conservation Office*

*Plug-in Hybrid Electric Vehicle Research Center*

*Institute of Transportation Studies*

### Special Thanks ###
*
Emerson David, Jessica Galvan, John Coon, Lisa Johnston, Jeremy Blonde, Lowell Valiant, Jan Carmikle, and Joules the Cow.*