/** 
Angular 2 service for providing Facilities Link building information to the app.
@author: Matt Sidor
@Last revision: 
@Last revised by: 
*/

import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { FacLink }           from './faclink.model';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FacLinkService {

    constructor (private http: Http) {}

    //server-side route wrapper for faclink api
    private allBuildingsUrl = '/api/facLink/allBuildings';

    //array of objects with alternate building names and FacLink keys
    private altNamesAndKeys = [
        { name: 'Rifle Range', key: 'DV-01-000158' },
    ];

    //'buildings' that are not in FacilitiesLink but that we need to track in Thermoostat
    private bonusBuildings = [
        { name: 'Peter A. Rock Hall', key: 'THERMOO-001' },
        { name: 'The Barn', key:'DV-01-000010' },        ///TODO: Fix CloverETL and add this to the csv list
		{ name: 'The Grove (Surge 3)', key:'DV-01-000463'}
    ];

    //alphabetizes array of objects by property
    dynamicSort(property:string) {																		//Pass in the JSON property that the array is to be sorted by. Putting a - in front of the string will set order to descending.
        let sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
			property = property.substr(1);																//Remove the (-) to continue sorting; Descending order has been stored
        }
        return function (a,b) {
            let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;		//Compares two variables and sorts accordingly
            return result * sortOrder;																	//Multiply by sortOrder flips from Ascending to Descending
        }
    }

	//Check array for alternate building names based on building key 
    checkAltNames(key) {
        for(let obj of this.altNamesAndKeys) {															//Custom alt names listed above
            if (key === obj.key) {
                return obj.name;
            }
        }
        return false;
    }

	//Trim the array to the name and key values to reduce amount of information to parse on front end.
    extractNamesAndKeys(originalArray) {
        let newArray = [];
        for(let i=0; i<originalArray.length; i++) {														//Step through the original data

            if(originalArray[i].primaryDisplay) {														//Ignore data that does not have a primaryDisplay name
                let name = this.checkAltNames(originalArray[i].bldgKey);								//Load Alt name if one exists
                if(!name) {
                    name = originalArray[i].primaryDisplay;												//Otherwise, use primaryDisplay
                }
                newArray.push({																			//Push into new array
                    name: name,
                    key: originalArray[i].bldgKey
                });
            }
        }
        newArray = newArray.concat(this.bonusBuildings);												//Add custom buildings to the array
        return newArray;																				
    }

    //returns observable of array of objects with names and building keys
    getAllBuildingNamesAndKeys() : Observable<FacLink[]> {
        return this.http.get(this.allBuildingsUrl)
            .map((res:Response) => res.json())
            .map(array => {
                let arrayOfNamesAndKeys = this.extractNamesAndKeys(array);								//Load cleaned array
                arrayOfNamesAndKeys.sort(this.dynamicSort('name'));										//Sort final array
                return arrayOfNamesAndKeys;
            })
            .catch((error:any) => Observable.throw(error.json().error || 'FacLink server error'));
    }

}