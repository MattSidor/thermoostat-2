import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/animations';
import { AppComponent } from '../app.component';

@Component({
  selector: 'modal-dialog',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scaleY(.3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scaleY(.0)' }))
      ])
    ])
  ]
})
export class DialogComponent implements OnInit {
  @Input() closable = true;
  @Input() mod:object;
  @Output() visibleChange: EventEmitter<object> = new EventEmitter<object>();
constructor(private appComp: AppComponent) {}

  ngOnInit() {
  }

close() {
	this.mod = {	
		showDialog : false,
		modalText: "",
		modalClass: "",
		overlay: "",
		guest: false
	};
    this.visibleChange.emit(this.mod);
  }
}