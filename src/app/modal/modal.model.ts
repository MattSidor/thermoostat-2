/** 
Exports related to the modal service.
*/

//Establishes the expected behavior of feedback components
export interface Modal {
    showDialog: boolean; 
    modalText: string;
    modalClass: string;
    overlay: string;
    guest: boolean;
    mapModal: object;
    closable: boolean;
}

//Construct for a feedback variable. Use with the 'new' keyword to create an instance with default values.
export function ModalConstruct () {
	this.props = {
		modalText: '',
		showDialog : false,
		modalClass: "",
		overlay: "",
		guest: false,
		closable: true
	}
	this.mapModal= {
		"overlay" : false,
		"ceed" : false, 
		"trimWaste" : false,
		"coolBuilding" : false
	};
};
