/** 
Angular 2 service for providing modal related information to the app.
@author: Mike Loranc
@Last revision: 
@Last revised by:
*/

import { Injectable } from '@angular/core';
import { ModalConstruct } from './modal.model';

@Injectable()
export class ModalService {
    constructor() { } 
    modal: any;

//Creates a new modal instance with default values
    initializeModal() {
        this.modal = new ModalConstruct();
    }

//Gathers existing modal
    getModal() {
        if(this.modal === undefined)								//Create new modal instance if no modal exists
            this.initializeModal();
        return this.modal;
    }
	
	//Save modal locally, and then post to database
    setModal(modalProps) {
        this.modal = Object.assign(this.modal,modalProps);			//Stores new modal changes/additions
    }
	
	clearModal() {
        if(this.modal === undefined)								//Create new modal instance if no modal exists
            return this.initializeModal();
		let mod = {
			props : {
				modalText : '',
				showDialog : false,
				modalClass : "",
				overlay : "",
				guest : false,
				closable : true
			},
			mapModal : {
				overlay : false,
				ceed : false,
				trimWaste : false,
				coolBuilding : false
			}			
		};
		this.setModal(mod);
		return this.modal;
	}
	
	guestAlert() {
        if(this.modal === undefined)								//Create new modal instance if no modal exists
            this.initializeModal();
		let mod = {
			props : {
				modalText : '',
				showDialog : true,
				modalClass : "modal-guest",
				overlay : "overlay",
				guest : true,
				closable : true
			}
		};
		this.setModal(mod);
		return this.modal;
	}
	
	guestReminder() {
		let mod = {
			props : {
				modalText : '<b>Guest session - your feedback will not be saved.</b>',
				showDialog : true,
				modalClass : "modal-reminder",
				overlay : "",
				guest : false,
				closable : false
			}
		};
		this.setModal(mod);
		return this.modal;
	}
		
	sessionExpired() {
		let mod = {
			props : {
				modalText : '<b>Your session has expired. Please try again.</b>',
				showDialog : true,
				modalClass : "modal-expired",
				overlay : "",
				guest : false,
				closable : true
			}
		};
		this.setModal(mod);
		return this.modal;
	}
	
	showMapModal() {
        if(this.modal === undefined)								//Create new modal instance if no modal exists
            this.initializeModal();
		this.clearModal();
		let ads = Object.keys(this.modal.mapModal);					//Dynamically get all the ads available
		ads.splice(ads.indexOf("overlay"), 1);						//Splice out the overlay property
		let randomAd = ads[Math.floor(Math.random()*ads.length)];	//Pick an advertisement for final page
		let mod = {
			mapModal : {
				overlay : true,
				[randomAd] : true
			}
		};
		this.setModal(mod);
		return this.modal;
	}    
}