export class FacLink {
    constructor(
        public bldgKey: string,
        public officialName: string,
        public primaryDisplay: string,
        public assetNumber: string,
        public facilitiesCode: string,
        public ucdhsBldgNum: string,
        public location: string,
        public affiliation: string,
        public category: string,
        public primaryUse: string,
        public address: string,
        public city: string,
        public county: string,
        public state: string,
        public zip: string,
        public countryCode: string,
        public addressCode: string,
        public latitude: string,
        public longitude: string,
        public occupied: string,
        public onOffCampus: string,
        public cefaName: string,
        public cefaFireResistConsTypCd: string,
        public planning: string,
        public condition: string,
        public defaultOMPElig: string,
        public ompEligMethod: string,
        public ownership: string,
        public floors: string,
        public height: string,
        public footprint: string,
        public perimeter: string,
        public basicGross: string,
        public cuGross: string,
        public circulation: string,
        public custodial: string,
        public mechanical: string,
        public parking: string,
        public toilet: string,
        public nasf: string,
        public asf: string,
        public spaceCount: string,
        public netUsable: string,
        public structural: string,
        public outsideGross: string,
        public relatedGross: string,
        public maintainedGross: string,
        public janitorized: string,
        public constructed: string,
        public renovated: string,
        public vacated: string,
        public demolished: string,
        public ompEligSummary: string
    ){}
}