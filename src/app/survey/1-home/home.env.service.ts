/** 
Angular 2 service for setting environment variables for the app.
@author: Mike Loranc
@Last revision: 
@Last revised by: 
*/

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EnvService {
    constructor(private http: Http) { }
	//Gets the hyperlink for CAS
	getLink() {
        return this.http.get('/api/cas/envLink/')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Environment Service Error'));
    };
}