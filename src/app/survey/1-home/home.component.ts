/** 
Angular 2 component for the homepage.
*/

import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '../../app.component';
import { EnvService } from './home.env.service';
import { ModalService } from '../../modal/modal.service';


@Component({
    selector: 'survey-home',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './home.component.html'
})


export class SurveyHomeComponent {

    public locUrl: any;

    constructor(
        public route: ActivatedRoute,
        private router: Router,
        private appComp: AppComponent,
        private envService: EnvService,
		private modalService : ModalService
    ) {}

    ngOnInit() {
        let url = this.router.url;												//Get the current URL
        let params = url.substring(url.indexOf('?')+1).split("&");				//Retreieve any query string parameters
        for(let i=0; i < params.length; i++){
            let item = params[i].split("=");
            if(item[0] === 'utm_campaign')
                sessionStorage.setItem('utm_campaign', item[1]);				//Store the utm_campaign if exists for tracking in the database
            if(item[0] === 'utm_source')
                sessionStorage.setItem('utm_source', item[1]);					//Store the utm_source if exists for tracking in the database
        }
	this.modalService.clearModal();
		if(url.indexOf('expired') != -1){										//If session was expired, show expired modal
				this.modalService.sessionExpired();
	console.log("Here");
		}
        this.envService.getLink().subscribe(data => this.locUrl = data);		//Set main button URL based on environment
    }
}