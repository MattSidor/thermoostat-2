import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';


@Injectable()
export class SummaryService {

    constructor(
        private http: Http
    ) {}

getBuildingSummary(buildingName:String, qrtr:String) : Observable<any> {
	let summaryUrl = '/api/summary/quarter/' + qrtr + '/' + buildingName;
	return this.http.get(summaryUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'FacLink server error'));
    }

    getMax(comfortSummary) {
        let comfortArray = [];
        for (let prop in comfortSummary) {
            comfortArray.push(comfortSummary[prop]);
        }
        return Math.max(...comfortArray);
    }
	
getComfortValues(buildingName:String, qrtr:String): Observable<any> {
        return this.getBuildingSummary(buildingName, qrtr)
            .map(summaryObject => {
                return {
                    cold: summaryObject.comfortSummary.cold,
                    chilly: summaryObject.comfortSummary.chilly,
                    perfect: summaryObject.comfortSummary.perfect,
                    warm: summaryObject.comfortSummary.warm,
                    hot: summaryObject.comfortSummary.hot,
                    max: this.getMax(summaryObject.comfortSummary),
					total: summaryObject.total
                }
            }
        );
    }
	
	//Retrieve quarter data for quarter selection dropdown
	getQrtrs() {
        return this.http.get('/api/map/quarter/')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Quarter Service Error'));
    };
}
