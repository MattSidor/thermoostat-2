/** 
Angular 2 component for the location page.
*/

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';
import { ModalService } from '../../modal/modal.service';

import { SummaryService } from './summary.service';

@Component({
  selector: 'summary',
  styleUrls: ['./summary.component.scss'],
  templateUrl: './summary.component.html'
})
export class SurveySummaryComponent {

  public feedback: Feedback;
  public svgParameters: any;
  public comfortValues: any;
  public moreFeedback: string;
  public qrtrs: any;
  public selectQrtr: any;

  constructor(
      private feedbackService: FeedbackService,
      public router: Router,
      private summaryService: SummaryService,
	  private modalService: ModalService
  ) {}

  ngOnInit() {
    //initialize variables
    this.svgParameters = {
      coldWidth: null,
      chillyWidth: null,
      perfectWidth: null,
      warmWidth: null,
      hotWidth: null
    };
    this.comfortValues = {
      cold: null,
      chilly: null,
      perfect: null,
      warm: null,
      hot: null,
      max: null,
	  total: null
    };
	  
	this.summaryService.getQrtrs()
		.subscribe(data => {
	  		this.qrtrs = data;															//Used for the quarter selection dropdown
			this.selectQrtr = this.qrtrs[0].name;	    								//set initial quarter to first object in array
			this.drawBars();
	}, err=>{console.log(err); });
	this.modalService.showMapModal();
    this.feedback = this.feedbackService.getFeedback();									//Load feedback for summary purposes
    this.moreFeedback = "/api/cas/";													//More feedback direction for logged in user
    if(this.feedback.casSession === null)
        this.moreFeedback = "/location";												//More feedback direction for guest user
      
    this.feedbackService.initializeFeedback();											//Reset feedback

  }

	//Returns the appropriate wording for number of votes
getVotes(num, adj){
	if(num == 1)
		return num + adj + " vote";
	else
		return num + adj + " votes";
}
getPercentage(num){
	return Math.round((num/this.comfortValues.total)*100) || 0;
}

drawBars(){
	this.summaryService.getComfortValues(this.feedback.building, this.selectQrtr).subscribe(				//Retrieve feedback for selected building
        comfortValues => {  
          this.comfortValues = comfortValues;
          let maxWidth = 160,															//Most votes will have a completely full bar
              maxWidthRatio = maxWidth / this.comfortValues.max;
          this.svgParameters = {														//Display feedback value bars as percentage lengths
            coldWidth: Math.round(this.comfortValues.cold*maxWidthRatio) || 0,
            chillyWidth: Math.round(this.comfortValues.chilly*maxWidthRatio) || 0,
            perfectWidth: Math.round(this.comfortValues.perfect*maxWidthRatio) || 0,
            warmWidth: Math.round(this.comfortValues.warm*maxWidthRatio) || 0,
            hotWidth: Math.round(this.comfortValues.hot*maxWidthRatio) || 0
          };
	}, err=>{console.log(err); });
}
	
}