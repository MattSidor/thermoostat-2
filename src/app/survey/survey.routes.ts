import { Routes } from '@angular/router';
import { SurveyHomeComponent } from './1-home';
import { SurveyLocationComponent } from './2-location';
import { SurveyComfortComponent } from './3-comfort';
import { SurveyCommentsComponent } from './4-comments';
import { SurveySummaryComponent } from './5-summary';

export const SurveyRoutes : Routes = [
  { path: '', component: SurveyHomeComponent },
  { path: 'home', component: SurveyHomeComponent },
  { path: 'location', component: SurveyLocationComponent },
  { path: 'comfort', component: SurveyComfortComponent },
  { path: 'comments', component: SurveyCommentsComponent },
  { path: 'summary', component: SurveySummaryComponent },
  { path: 'expired', component: SurveyHomeComponent }
];
