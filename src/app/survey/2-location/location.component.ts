/** 
Angular 2 component for the location page.
*/

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';
import { FacLinkService } from '../../faclink.service';
import { ModalService } from '../../modal/modal.service';

import 'date-input-polyfill';

import * as $ from 'jquery';
import 'corejs-typeahead';

@Component({
  selector: 'location',
  styleUrls: ['./location.component.scss'],
  encapsulation: ViewEncapsulation.None,
  templateUrl: './location.component.html'
})
export class SurveyLocationComponent implements OnInit {

  public feedback: Feedback;
  public location: any;
  public buildingNameInput: any;
  public buildingMatch: boolean;
  public facLinkBuildings: any;
  private dirtyBuilding: boolean;
  // public typingTimer: any;

  public roomTextShown: boolean;

  constructor(
      private feedbackService: FeedbackService,
      private faclinkService: FacLinkService,
      private router: Router,
	  private modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.feedback = this.feedbackService.getFeedback();													//Set feedback to existing feedback
    this.location = {																					//Set location object for submissions
      building: this.feedback.building,
      buildingKey: this.feedback.buildingKey,
      room: this.feedback.room,
      notCurrentlyHere: this.feedback.notCurrentlyHere,
      previouslyHereDate: this.feedback.previouslyHereDate,
      previouslyHereTime: this.feedback.previouslyHereTime,
      utm_campaign: sessionStorage.getItem('utm_campaign') || this.feedback.utm_campaign,
      utm_source: sessionStorage.getItem('utm_source') || this.feedback.utm_source,
      casSession : this.feedback.casSession  
    };

    this.buildingNameInput = this.feedback.building;
    this.buildingMatch = false;
    this.loadBuildings();																				//Load array of buildings for typeahead
    let url = this.router.url;																			//Get the URL
    let session = url.substring(url.indexOf('=')+1);													//Store the session if one exists
    if(session === url)
        session = null;																					//If session does not exist, set session to null for 'Guest Mode'
    this.location.casSession = session;
      
    if(url.indexOf('session') === -1)																	//If session does not exist, show Guest alert modal
		this.modalService.guestAlert();
  }

  substringMatcher (stringsArray) {
    return function findMatches(query, syncResults) {
        let matches = [],   //array to store matched regex strings
            substringRegex = new RegExp(query, 'i');
        // regex used to determine if a string contains the query
        // 'i' means case-insensitive

        // iterate through the pool of strings and for any string that
        // contains the substring `query`, add it to the `matches` array
        stringsArray.forEach(function (string) {
            if (substringRegex.test(string)) {
                matches.push(string);
            }
        });

        //sort matches to give precedence to strings with same letter order
        //(e.g. "ac" sorts "ACademic Surge" over "Arboretum TeAChing Nursery")
        matches.sort(function(a,b) {
            let begginingA = a.substr(0,query.length).toLowerCase();
            let begginingB = b.substr(0,query.length).toLowerCase();

            if (begginingA == query.toLowerCase()) {
                if (begginingB != query.toLowerCase()) return -1;
            } else if (begginingB == query.toLowerCase()) return 1;
            return a < b ? -1 : (a > b ? 1 : 0);
        });
        
        syncResults(matches);
    };
  };

	//Gets building through the Facilities Link Service that gathers data from the API
  loadBuildings(): void {
    this.faclinkService.getAllBuildingNamesAndKeys()
        .subscribe(
            facLinkBuildings => {
                this.facLinkBuildings = facLinkBuildings;   											// Bind to view
                let buildingNames = Object.keys(facLinkBuildings).map(function (k) {
                    return facLinkBuildings[k].name;
                });

                let typeaheadElement = $('#typeahead');													//initialize typeahead component on input field with id 'typeahead'
                typeaheadElement.typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'buildings',
                    source: this.substringMatcher(buildingNames)
                });

                //attach event listener to typehaed:select event
                let outerThis = this;
                typeaheadElement.bind('typeahead:select', function(ev, suggestion) {
                    outerThis.checkBuildingValidity();
                });

                this.checkBuildingValidity();
            },
                err => { console.log(err); }
            );
  }

//Checks if entry is valid against the Facilities Link Building array
  checkBuildingValidity() {

      let typeaheadValue = $('#typeahead').typeahead('val');
      if(typeaheadValue) {
          this.buildingNameInput = typeaheadValue;
      }

      let buildingSearch = this.facLinkBuildings.find(building => building.name === this.buildingNameInput); //uses Array.find to search list of buildings
      if(buildingSearch) {  																			//true if match was found
        this.location.building = buildingSearch.name;
        this.location.buildingKey = buildingSearch.key;
        this.buildingMatch = true;
      }else { 																							//false if match was not found
        this.location.building = null;
        this.location.buildingKey = null;
        this.buildingMatch = false;
      }
  }

//Sets the style on Building field
getStyle(classes){
	if(this.dirtyBuilding)
		classes+=" dirty-building";																		//Used to set the building field to dirty after someone types. Remains if they then clear the field as an invalid data check.
    if(this.buildingMatch)
        return classes + " valid-data";
    else
        return classes + " invalid-data";
}

//Sets dirty building
setBuildingStyle(){
	this.dirtyBuilding = !(this.buildingMatch);															//Gets called by onBlur to allow the user to finish typing before establishing invalid data
}

//Binding for required property on Date and Time
hereCheck(){
    return this.location.notCurrentlyHere;
}

//Posts data from the for to the Feedback service
postInput() {
    sessionStorage.clear();
    this.feedbackService.appendFeedback(this.location)													//Sends stored location data from the forms   
        .subscribe(
        ans => {
            if(ans==="Session error")																	//If session has expired, send user back to homepage to start a new session
                this.router.navigate(['expired']);
            else
                this.router.navigate(['comfort']);
        },
        err => { console.log(err); });   
}
	
//Checks validity of the form before submission
save(model, isValid: boolean) {
    if(isValid) 
        this.postInput();
}
}