/** 
Angular 2 component for the comfort page.
*/

import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';
import { ModalService } from '../../modal/modal.service';

@Component({
  selector: 'comfort',
  styleUrls: ['./comfort.component.scss'],
  encapsulation: ViewEncapsulation.None,
  templateUrl: './comfort.component.html' 
})
export class SurveyComfortComponent {

  public feedback: Feedback;
  public comfortPage: any;
  private prev: string = null;
    
  constructor(
      private feedbackService: FeedbackService,
      public router: Router,
	  private modalService: ModalService
  ) {}

  ngOnInit() {
    this.feedback = this.feedbackService.getFeedback();				//Load existing feedback
    this.comfortPage = {
      comfort: this.feedback.comfort,
      activity: this.feedback.activity,
      started: this.feedback.started,
	  satisfaction: this.feedback.satisfaction
    };
	  
    if(this.feedback.casSession==null)								//If Guest session, display Guest session modal alert
		this.modalService.guestReminder();
  }
    
//Submit comfort feedback to the Feedback Service
postInput() {
    this.feedbackService.appendFeedback(this.comfortPage)    
        .subscribe(
        ans => {
            if(ans==="Session error")
                this.router.navigate(['expired']);					//if session has expired, send user back to homepage
            else
                this.router.navigate(['comments']);
        },
        err => { console.log(err); });   
}

//Check that a comfort value has been selected for displaying recency selection
checkComfort() {
    if(this.comfortPage.comfort !=null)
        return false;												//Display
    return true;													//Not selected, keep hidden
}

//Sets green border and checkmark when a level of physical activity is selected
getActivity() {
    if(this.comfortPage.activity !="")
        return "activitySelected";
    return "activity";
}

//Sets the green border around recency selection
getStarted() {
    if(this.comfortPage.started !="")
        return "startedSelected";
    return "started";
}
	
unselect(event) {
	if(this.prev==event.target.id){
		(<HTMLInputElement>document.getElementById(event.target.id)).checked = false;
		this.comfortPage.satisfaction="";
	}
	this.prev=this.comfortPage.satisfaction;
}

//Check validity before submission
  save(model, isValid: boolean) {
    if(isValid)
        this.postInput();
  }
}
