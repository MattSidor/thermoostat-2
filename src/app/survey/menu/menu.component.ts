import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
    selector: 'app-menu',
    styleUrls: ['./menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './menu.component.html'
})
export class MenuComponent {

    routerSubscription: Subscription;
    routeTitle: string;
    appsClickState: boolean = false;
    routeTitleHoldBox: string;

    //DOM element references, which will show/hide and grow/shrink based on user interactions
    @ViewChild('apps') appsElement:ElementRef;
    @ViewChild('info') infoElement:ElementRef;
    @ViewChild('backButton') backButtonElement:ElementRef;
    @ViewChild('appsButton') appsButtonElement:ElementRef;
    @ViewChild('infoButton') infoButtonElement:ElementRef;
    @ViewChild('title') titleElement:ElementRef;

    constructor(
      public router: Router
    ) {}

    //show or hide apps menu on click, and make sure info menu is hidden either way
    appsClick() {
        if(this.appsClickState === false) {
            this.appsElement.nativeElement.hidden = false;
            this.infoElement.nativeElement.hidden = true;

            this.titleElement.nativeElement.hidden = true;
            this.backButtonElement.nativeElement.hidden = false;
            this.infoButtonElement.nativeElement.style.visibility = 'hidden';   //using 'visibility' property to maintain flexbox spacing

            this.appsButtonElement.nativeElement.style.cursor = 'unset';

          this.appsButtonElement.nativeElement.style.display = null;

          this.appsClickState = true;
        }
    }
	
	appsButtonClass() {
		if(this.appsClickState)
			return "apps-true";
		return "apps-button";
	}
	
    //show or hide info menu on click, and make sure apps menu is hidden either way
    infoClick() {
        this.infoElement.nativeElement.hidden = !this.infoElement.nativeElement.hidden;
        this.appsElement.nativeElement.hidden = true;
        this.infoButtonElement.nativeElement.style.visibility = 'hidden';   //using 'visibility' property to maintain flexbox spacing
        this.backButtonElement.nativeElement.hidden = false;
        this.appsButtonElement.nativeElement.hidden = true;
        this.appsButtonElement.nativeElement.style.display = 'none';

        this.routeTitleHoldBox = this.routeTitle;
        this.routeTitle = 'About';
    }

    //hide both apps and info menus, and the back button
    //show infoButton and title again if hidden
    //reset appsClickState boolean and cursor style for that div
    //reset routeTitle to 'TherMOOstat'
    hideMenus() {
        this.appsElement.nativeElement.hidden = true;
        this.infoElement.nativeElement.hidden = true;
        this.backButtonElement.nativeElement.hidden = true;
        this.appsButtonElement.nativeElement.hidden = false;
        this.appsButtonElement.nativeElement.style.display = 'inline';

      this.infoButtonElement.nativeElement.style.visibility = 'initial';
        this.titleElement.nativeElement.hidden = false;

        this.appsClickState = false;
        this.appsButtonElement.nativeElement.style.cursor = 'pointer';

        if(this.routeTitle) {   //check if routeTitle has a value first before attempting to set it
            if(this.routeTitleHoldBox) {
                this.routeTitle = this.routeTitleHoldBox;
            }else {
                this.routeTitle = `<b>TherMOOstat</b>`;
            }
        }
    }

    ngOnInit() {
        //subscribe to router events; change this.routeTitle when NavigationEnd event triggers (meaning a new route has been loaded)
        this.routerSubscription = this.router.events.subscribe((event:Event) => {
            if (event instanceof NavigationEnd) {
                let routePath = event.url.substr(0, (event.url + "?").indexOf("?"));    //parse out route url before question mark (e.g. '/location?session=...')
                switch (routePath) {
                    case '/location':
                        this.routeTitle = '1 of 3: Locate';
                        break;
                    case '/comfort':
                        this.routeTitle = '2 of 3: Comfort';
                        break;
                    case '/comments':
                        this.routeTitle = '3 of 3: Review';
                        break;
                    case '/summary':
                        this.routeTitle = 'Thank you!';
                        break;
                    default:
                        this.routeTitle = `<b>TherMOOstat</b>`;
                        break;
                }
            }
        });
    }

    ngAfterViewInit() {
        this.hideMenus();   //hide both apps and info menus when DOM first loads
    }
}