/** 
Angular 2 service for providing feedback related information to the app.
@author: Matt Sidor
@Last revision: 3/14/17 - Reworked behavior of the service.
@Last revised by: Mike Loranc
*/

import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { FeedbackConstruct } from './feedback';

@Injectable()
export class FeedbackService {
    constructor(private http: Http) { } 
    feedback: any;

//Creates a new feedback instance with default values
    initializeFeedback() {
        this.feedback = new FeedbackConstruct();
    }

//Gathers existing feedback
    getFeedback() {
        if(this.feedback === undefined)															//Create new feedback instance if no feedback exists
            this.initializeFeedback();
        return this.feedback;
    }

//Save feedback locally, and then post to database
    appendFeedback(feedbackPiece) {
        this.feedback = Object.assign(this.feedback,feedbackPiece);								//Stores new feedback changes/additions
        return this.http.post('/api/feedback/post/'+this.feedback.casSession, this.feedback)	//Posts update to database
        .map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Session error'));
    }
    
}