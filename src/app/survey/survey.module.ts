import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';

import { InlineSVGModule } from 'ng-inline-svg';
import { Angulartics2Module, Angulartics2GoogleAnalytics } from 'angulartics2';

import { RouterModule } from '@angular/router';
import { SurveyRoutes } from './survey.routes';

import { FeedbackService } from './feedback.service';

import { FacLinkService } from '../faclink.service';
import { ModalService } from '../modal/modal.service';
import { EnvService } from './1-home/home.env.service';

import { MenuComponent } from './menu';
import { SurveyComponent } from './survey.component';
import { SurveyHomeComponent } from './1-home';
import { SurveyLocationComponent } from './2-location';
import { SurveyComfortComponent } from './3-comfort';
import { SurveyCommentsComponent } from './4-comments';
import { SurveySummaryComponent } from './5-summary';

import { SummaryService } from './5-summary/summary.service';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(SurveyRoutes, { useHash: false }),
    Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ]),
    InlineSVGModule
  ],
  declarations: [
    SurveyComponent,
    MenuComponent,
    SurveyHomeComponent,
    SurveyLocationComponent,
    SurveyComfortComponent,
    SurveyCommentsComponent,
    SurveySummaryComponent
  ],
  providers: [
    FeedbackService,
    FacLinkService,
    SummaryService,
    EnvService,
	ModalService
  ],
  exports:      [ SurveyComponent ]
})
export class SurveyModule { }
