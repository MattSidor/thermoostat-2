import { Component } from '@angular/core';
import { Angulartics2GoogleAnalytics } from 'angulartics2';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
    selector: 'survey',
    styleUrls: ['./survey.component.scss'],
    template: `<app-menu></app-menu>
              <router-outlet></router-outlet>`
})
export class SurveyComponent {

    routerSubscription: Subscription;

    constructor(
        angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
        private router: Router
    ) {}

    ngOnInit() {

        let surveyElement = document.getElementById('survey');

        this.routerSubscription = this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe(event => {
                surveyElement.scrollTop = 0;
            });
    }

}
