/** 
Angular 2 component for the comments page.
*/

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';
import { ModalService } from '../../modal/modal.service';

@Component({
  selector: 'comments',
  styleUrls: ['./comments.component.scss'],
  templateUrl: './comments.component.html'
})
export class SurveyCommentsComponent {

  public feedback: Feedback;
  public commentsPage: any;
  public comfortIconUrl: string;
  public rewordedActivity: string;
  private clothingDescription: string;
  private prev: any = {
	  clothing:null,
	  activity:null
	};

  constructor(
      private feedbackService: FeedbackService,
      public router: Router,
	  private modalService: ModalService
  ) {}

  ngOnInit() {
    this.feedback = this.feedbackService.getFeedback();										//Load existing feedback
    if(this.feedback.comfort == null)														//If user finished survey and used back button, send them back to the homepage for a new session
        this.router.navigate(['expired']);
    this.comfortIconUrl = '/assets/images/comfort-icons/'+this.feedback.comfort+'-review.svg';

    this.commentsPage = {
      comment: this.feedback.comment,
		clothing: this.feedback.clothing,
		activity: this.feedback.activity,
      satisfaction: this.feedback.satisfaction,
      emailConfirmation: this.feedback.emailConfirmation,
      isComplete: true																		//Mark as complete for final step on submission
    };

    if(this.feedback.activity === 'none') {
        this.rewordedActivity = 'No';														//Conversion of text for activity level set to 'none'
    }else {
        this.rewordedActivity = this.feedback.activity.replace(
            /\b[a-z]/g, function(f){        												//regex to capitalize first letter
                return f.toUpperCase();
            });
    }
    if(this.feedback.casSession==null)														//If Guest session, display Guest session modal alert
		this.modalService.guestReminder();

  }
//Sends comments to the feedback service
  postInput() {
      this.feedbackService.appendFeedback(this.commentsPage)
          .subscribe(
          ans => {
              if(ans==="Session error")
                  this.router.navigate(['expired']);										//If session has expired, send user back to homepage to start a new session
              else
                  this.router.navigate(['summary']);
          },
          err => { console.log(err); });
  }

//Sets the comments field class based on input	
  getComment() {
      if(this.commentsPage.comment !="")
          return "commentAdded";
      return "comment";
  }

//Sets the satisfaction field class based on selection
  getSatisfaction() {
      if(this.commentsPage.satisfaction !="")
          return "satisfactionSelected";
      return "satisfaction";
  }
	
unselect(event) {
	if(this.prev.clothing==event.target.id){
		(<HTMLInputElement>document.getElementById(event.target.id)).checked = false;
		this.commentsPage.clothing="";
	}
	if(this.prev.activity==event.target.id){
			(<HTMLInputElement>document.getElementById(event.target.id)).checked = false;
			this.commentsPage.activity="";	
	}
	this.prev.clothing = this.commentsPage.clothing;
	this.prev.activity = this.commentsPage.activity;
}

//All fields are optional, so submits feedback on click. Maintained for consistency.
  save(model, isValid: boolean) {
        this.postInput();
  }
}
