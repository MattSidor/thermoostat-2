/** 
Exports related to the feedback service.
*/

//Establishes the expected behavior of feedback components
export interface Feedback {
    casSession?: string; 
    building?: string;
    buildingKey?: string;
    room?: string;
    notCurrentlyHere?: boolean;
    previouslyHereDate?: string;
    previouslyHereTime?: string;
    comment?: string;
    started?: string;
    isFavorite?: boolean;
    utm_campaign?: string;
    utm_source?: string;
    comfort?: string;
	clothing?: string;
    satisfaction?: string;
    activity?: string;
    emailConfirmation?: boolean;
    isComplete?: boolean;
}

//Construct for a feedback variable. Use with the 'new' keyword to create an instance with default values.
export function FeedbackConstruct () {
	this.casSession = null;
	this.building= "";
	this.buildingKey= "";
	this.room= "";
	this.notCurrentlyHere= false;
	this.previouslyHereDate= "";
	this.previouslyHereTime= "";
	this.comfort= null;
	this.clothing= "";
	this.activity= "";
	this.comment= "";
	this.satisfaction= "";
	this.started= "";
	this.utm_campaign= "";
	this.utm_source= "";
	this.emailConfirmation= false;
	this.isComplete= false;
};
