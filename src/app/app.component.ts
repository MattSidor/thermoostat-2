/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation } from '@angular/core';

import * as runtime from 'serviceworker-webpack-plugin/lib/runtime';

import { MapDataService } from './map/map.data.service';
import { Modal } from './modal/modal.model';
import { ModalService } from './modal/modal.service';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.scss', ],
  templateUrl: './app.component.html'
})
export class AppComponent {
    name = 'TherMOOstat';
	public modal: Modal;
//	public mapData: any;
	
constructor(private modalService: ModalService) {}

  ngOnInit() {
    //serviceworker registration via serviceworker-webpack-plugin library
    if ('serviceWorker' in navigator) {
      const registration = runtime.register();
    }
//	this.mapData = this.mapDataService.getMap();  
	this.modal = this.modalService.getModal();
  }

}
