import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
// import { trigger, style, transition, animate } from '@angular/animations';
import { MapComponent } from '../map.component';

@Component({
  selector: 'map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent implements OnInit {
  @Input() mapClosable = true;
  @Input() mapVisible;
  @Output() mapVisibleChange: EventEmitter<object> = new EventEmitter<object>();
constructor(private mapComp: MapComponent) { }

  ngOnInit() {
  }
sendToCEED() {
    window.open('https://ceed.ucdavis.edu/#!/?utm_source=TherMOOstat&utm_campaign=summary_page','_blank');
}
	
sendToTrimWaste() {
	window.open('http://trimthewaste.ucdavis.edu/','_blank');
}
	
  close() {
	for(let prop in this.mapVisible)
		this.mapVisible[prop] = false;
    this.mapVisibleChange.emit(this.mapVisible);
  }
}