import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Map} from "leaflet";

@Injectable()
export class MapJSService {
    public map: Map;
    public baseMaps: any;

    constructor(private http: Http) {
        this.baseMaps = {
            Thermoostat: L.tileLayer("https://api.mapbox.com/styles/v1/ucd-eco/cim3tvp7200eb9jkpfp13nclo/tiles/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidWNkLWVjbyIsImEiOiI3M3Axa3UwIn0.wdzUdL1mxI2FC4ILZ3YPRg", {
                attribution: '&copy; <a href="https://www.mapbox.com/about/maps/">Mapbox</a> &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                tileSize: 512,
                zoomOffset: -1
            })
        };
    }

    // disableMouseEvent(elementId: string) {
    //     let element = <HTMLElement>document.getElementById(elementId);
    //
    //     L.DomEvent.disableClickPropagation(element);
    //     L.DomEvent.disableScrollPropagation(element);
    // }
}
