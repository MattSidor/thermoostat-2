/** 
Angular 2 service for providing map data to the app.
@author: Mike Loranc
@Last revision: 
@Last revised by:
*/

import { Injectable } from '@angular/core';
import { MapDataConstruct } from './map.data';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class MapDataService {
	private weatherUrl = '/api/map/temperature';
	constructor(private http: Http) {} 
    mapDraw: any;
//Creates a new map instance with default values
    initializeMap() {
        this.mapDraw = new MapDataConstruct();		
    }

//Gathers existing map data
    getMap() {
        if(this.mapDraw === undefined)
            this.initializeMap();
        return this.mapDraw;
    }
	
//Sets new map data.
	setMap(mapChange) {
		this.mapDraw = Object.assign(this.mapDraw, mapChange);
	}
	
//Gets temperature data
getWeather() {
	return this.http.get(this.weatherUrl)
	.map(res => res.json())
	.catch((error: any) => Observable.throw(error.json().error || 'Weather Service Error'));
};
	
//Provides tooltip buildings
	getTooltipBuildings(){
		return ['Wellman Hall',
				'Hoagland Hall',
				'Shields Library',
				'Mrak Hall',
				'King Hall',
				'Academic Surge Building',
				'Meyer Hall',
				'Memorial Union',
				'Segundo Dining Commons',
				'Activities and Recreation Center',
				'Tercero Community',
				'Sciences Lab Building',
				'Conference Center',
				'Plant & Environmental Sciences',
				'Valley Hall',
				'Genome & Biomedical Sciences Facility',
				'Bainer Hall',
				'Kemper Hall',
				'Vet Med 3A',
				'Olson Hall'
		];
	}
}