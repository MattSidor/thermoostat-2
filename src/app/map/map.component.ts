/** 
Main map component elements are set.
@author: Matt Sidor
@Last revision: 3/23/17 - Converted Quarter Selection into automated service.
@Last revised by: Mike Loranc
*/

import { Component, ViewEncapsulation, Input, NgModule } from '@angular/core';

import { MapJSService } from './map-js.service';
import { MarkerService } from './marker.service';
import { AppComponent } from '../app.component';
import { MapDataService } from './map.data.service';
import { MapDataPopup } from './map.data';

@Component({
  selector: 'map',
  providers: [MapJSService, MarkerService, MapDataService, MapDataPopup],
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None,  							//fixes inability to restyle <canvas> after mapbox-gl inserts this
  templateUrl: './map.component.html'
})

export class MapComponent {

  public quarters: any;
  public selectedQuarter: any;
  public mapBuild: any;
  public weather: any = {
		temperature : 0,
	    condition: " "
	};
	
  constructor(
      private mapJSService: MapJSService,
      private markerService: MarkerService, 
      private appComp: AppComponent,
	  private mapData: MapDataService
  ) {}

  ngOnInit() {
	this.markerService.getQuarters()
		.subscribe(data => {
	  		this.quarters = data;									//Used for the quarter selection dropdown
			this.selectedQuarter = this.quarters[0].name;	    	//set initial quarter to first object in array
			this.markerService.drawMarkers(this.selectedQuarter);	//Initial marker drawing
	}, err=>{console.log(err); });
	this.checkWeather();
	this.updateWeather();
	this.mapBuild = this.mapData.getMap();
    let map = L.map("map", {										//Sets the default parameters for the map view
      zoomControl: this.mapBuild.zoomControl,
      center: L.latLng(this.mapBuild.lat, this.mapBuild.lng),
      zoom: this.mapBuild.zoom,
      minZoom: this.mapBuild.minZoom,
      maxZoom: this.mapBuild.maxZoom,
      layers: [this.mapJSService.baseMaps.Thermoostat]
    });
	  
    L.control.zoom({ position: "bottomright" }).addTo(map);			// +/- default zoom control placement
    this.mapJSService.map = map;

  }

	
  onQuarterChange() {												//Redraws the map when a new quarter is selected from dropdown
    this.markerService.drawMarkers(this.selectedQuarter);
  }

  checkWeather () {
		this.mapData.getWeather()
		.subscribe(data => {
			this.weather = data;
		}, err=>{console.log(err);});
  }
	
  updateWeather() {
    setInterval (() => {
		this.checkWeather();
    }, 30000)														//Checks the temperature every 30 seconds.
  }

}