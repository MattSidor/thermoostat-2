//Construct for the default map parameters.
export function MapDataConstruct () {
      this.zoomControl = false;
	  this.lat = 38.541;
	  this.lng = -121.753;
      this.zoom = 16;
      this.minZoom = 14;
      this.maxZoom = 18;
};

//Data store for the map. This class maintains dynamic html elements.
export class MapDataPopup {
	constructor(){}
	getIcon(height, width) {
		return `<svg xmlns="http://www.w3.org/2000/svg" viewBox="228.5 204.6 49.4 71.5" width="${width}" height="${height}">
				 <filter width="200%" height="200%" id="a" filterUnits="objectBoundingBox" y="-50%" x="-50%">
				   <feMorphology result="shadowSpreadOuter1" in="SourceAlpha" operator="dilate" radius=".7"/>
				   <feOffset dy="2" result="shadowOffsetOuter1" in="shadowSpreadOuter1"/>
				   <feGaussianBlur stdDeviation="3" result="shadowBlurOuter1" in="shadowOffsetOuter1"/>
				   <feComposite in2="SourceAlpha" result="shadowBlurOuter1" in="shadowBlurOuter1" operator="out"/>
				   <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.238564312 0" in="shadowBlurOuter1"/>
				 </filter>
				 <path d="M282.2 200.3c-13.2 0-24 10.6-24 24 0 13 23.4 45.7 23.4 45.7s24.6-32.7 24.6-45.8c0-13.3-10.8-24-24-24z" filter="url(#a)" transform="translate(-29 5)"/>
				 <path d="M253.2 205.3c-13.2 0-24 10.6-24 24 0 13 23.4 45.7 23.4 45.7s24.6-32.7 24.6-45.8c0-13.3-10.8-24-24-24z" class="marker-outline" stroke="#fff" stroke-width="1.4"/>
			   </svg>`;
	}

	getDefaultPopup(data, maxVote, qrtr) {
		let singularOrPlural:String = null;
		if(data.sum > 1) {
			singularOrPlural = 'votes';
		}else {
			singularOrPlural = 'vote';
		}

		let maxWidth = 130,															//Comfort value with most votes will have a completely full bar
		  maxWidthRatio = maxWidth / data.max;
		let svgParameters = {														//Display feedback value bars as percentage lengths
			coldWidth: Math.round(data.comfortSummary.cold*maxWidthRatio) || 0,
			chillyWidth: Math.round(data.comfortSummary.chilly*maxWidthRatio) || 0,
			perfectWidth: Math.round(data.comfortSummary.perfect*maxWidthRatio) || 0,
			warmWidth: Math.round(data.comfortSummary.warm*maxWidthRatio) || 0,
			hotWidth: Math.round(data.comfortSummary.hot*maxWidthRatio) || 0
		};

		return `<div class="building-popup ${data.className}">
				<h2>${data.name}</h2>
				<div class="details-and-graph-wrapper">
					<div class="details">
					  <b>${data.sum} ${singularOrPlural}</b><br>
					  ${qrtr}
					  <div class="percent-vote">${maxVote}% ${data.className}</div>
					</div>
					<div class="graph">
					  <div class="graph-row cold">
						<div class="value">${data.comfortSummary.cold}</div>
						<svg width="${svgParameters.coldWidth}" height="7" xmlns="http://www.w3.org/2000/svg">
						  <rect width="${svgParameters.coldWidth}" height="7" />
						</svg>
					  </div>
					  <div class="graph-row chilly">
						<div class="value">${data.comfortSummary.chilly}</div>
						<svg width="${svgParameters.chillyWidth}" height="7" xmlns="http://www.w3.org/2000/svg">
						  <rect width="${svgParameters.chillyWidth}" height="7" />
						</svg>
					  </div>
					  <div class="graph-row perfect">
						<div class="value">${data.comfortSummary.perfect}</div>
						<svg width="${svgParameters.perfectWidth}" height="7" xmlns="http://www.w3.org/2000/svg">
						  <rect width="${svgParameters.perfectWidth}" height="7" />
						</svg>
					  </div>
					  <div class="graph-row warm">
						<div class="value">${data.comfortSummary.warm}</div>
						<svg width="${svgParameters.warmWidth}" height="7" xmlns="http://www.w3.org/2000/svg">
						  <rect width="${svgParameters.warmWidth}" height="7" />
						</svg>
					  </div>
					  <div class="graph-row hot">
						<div class="value">${data.comfortSummary.hot}</div>
						<svg width="${svgParameters.hotWidth}" height="7" xmlns="http://www.w3.org/2000/svg">
						  <rect width="${svgParameters.hotWidth}" height="7" />
						</svg>
					  </div>
					</div>
				</div>
			</div>`;
	}
	
	getLabel(data){
		return `<div class="building-label hidden">
				  ${data.name}
				</div>`
	}

	getTooltip(data, maxVote){
		let singularOrPlural:String = null;
		if(data.sum > 1) {
			singularOrPlural = 'votes';
		}else {
			singularOrPlural = 'vote';
		}

		return `<div class="building-tooltip ${data.className}">
					<h2>${data.name}</h2>
					${data.sum} ${singularOrPlural} this quarter<br>
					<span>${maxVote}% ${data.className}</span>
				</div>`
	}
}