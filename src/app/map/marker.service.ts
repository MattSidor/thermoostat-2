/** 
Angular 2 service for providing map marker related information to the app.
@author: Matt Sidor
@Last revision: 4/11/17 - Marker popups.
@Last revised by: Matt Sidor
*/

import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';

import { Map } from "leaflet";
import { MapJSService } from "./map-js.service";
import { MouseEvent } from "leaflet";
import { MapDataService } from "./map.data.service";
import { MapDataPopup } from "./map.data";

import { Observable } from 'rxjs/Rx';


@Injectable()
export class MarkerService {
    public map: Map;
    private allBuildingsUrl = '/api/facLink/allBuildings';
    private currentQuarter: String;
    private markersArray: any;
    private popupsArray: any;

    constructor(
        private mapService: MapJSService,
        private http: Http,
		private mapDataService: MapDataService,
		private mapPopup: MapDataPopup
    ) {}

	private listOfPermanentTooltipBuildings: string[] = this.mapDataService.getTooltipBuildings();
    private currentZoom: number;

    extractNamesKeysAndLocations(originalArray) {
        let newArray = [];
        for(let i=0; i<originalArray.length; i++) {

            if(originalArray[i].primaryDisplay) {
                newArray.push({
                    name: originalArray[i].primaryDisplay,
                    key: originalArray[i].bldgKey,
                    latitude: originalArray[i].latitude,
                    longitude: originalArray[i].longitude
                });
            }

            //TODO: alt names??
        }
        return newArray;
    }

    //uses forkJoin of two obsrvables, one for FacilitiesLink data and one for quarterly TherMOOstat data
    //TODO: refactor into separate, logical services
    getMarkerData(quarter) {
        this.currentQuarter = quarter;

        return Observable.forkJoin(

            //Observable 1 - FacilitiesLink
            this.http.get(this.allBuildingsUrl)
                .map((res:Response) => res.json())
                .map(array => {
                    return this.extractNamesKeysAndLocations(array);
                })
                .catch((error:any) => Observable.throw(error.json().error || 'FacLink server error')),

            //Observable 2 - TherMOOstat Summary by Quarter
            this.http.get('/api/summary/quarter/'+this.currentQuarter).map((res:Response) => res.json())

        );
    };

	//Retrieve quarter data for quarter selection dropdown
	getQuarters() {
        return this.http.get('/api/map/quarter/')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Quarter Service Error'));
    };

    findStatistics(arrayOfObjects) {
        let arrayOfSums = [];
        let objectOfSums = {};
        let objectOfMaxes = {};

        for (let object of arrayOfObjects) {
            let sum = 0;
            let comfortMax = 0;

            for (let prop in object.comfortSummary) {
                sum += object.comfortSummary[prop];
                if (object.comfortSummary[prop] > comfortMax) {
                    comfortMax = object.comfortSummary[prop];
                }
            }
            arrayOfSums.push(sum);
            objectOfSums[object.buildingName] = sum;
            objectOfMaxes[object.buildingName] = comfortMax;
        }

        let min = Math.min(...arrayOfSums),
            max = Math.max(...arrayOfSums);

        return {
            min: min,
            max: max,
            range: max-min,
            allSums: objectOfSums,
            allMaxes: objectOfMaxes
        };
    }

    findMarkerClass(comfortObject) {

        // 1) re-weigh feedback so cold/perfect/hot are doubled
        let reweightedCold = comfortObject.cold * 2,
            reweightedChilly = comfortObject.chilly,
            reweightedPerfect = comfortObject.perfect * 2,
            reweightedWarm = comfortObject.warm,
            reweightedHot = comfortObject.hot * 2;

        // 2) add cold/chilly and warm/hot
        let coolSum = reweightedCold + reweightedChilly,
            heatSum = reweightedWarm + reweightedHot;

        if (reweightedPerfect === heatSum && reweightedPerfect === coolSum) {
            return 'tie';
        } else if (reweightedPerfect > coolSum && reweightedPerfect > heatSum) {  // 3) compare reweightedPerfect to coolSum and heatSum
            return 'perfect';
        } else if (heatSum > coolSum) {  // 4) compare heatSum to coolSum
            if (reweightedWarm > reweightedHot) {
                return 'warm';
            }else {
                return 'hot';
            }
        } else if (coolSum > heatSum) {  // 5) compare coolSum to heatSum
            if (reweightedChilly > reweightedCold) {
                return 'chilly';
            }else {
                return 'cold';
            }
        } else {  // 6) all other cases
            if (reweightedCold > reweightedHot) {
                return 'cold';
            }else if (reweightedHot > reweightedCold) {
                return 'hot';
            }else {
                return 'tie';
            }
        }
    }

    resetMapLabels(e: MouseEvent, markersArray) {
        this.currentZoom = e.target._zoom;

        if(this.currentZoom >= 18) {
            for (let marker of markersArray) {
                if(!this.listOfPermanentTooltipBuildings.includes(marker.options.alt)) {
                    marker.getElement().classList.remove('label-hidden');
                }
            }
        }else if(this.currentZoom <= 15) {
            for (let marker of markersArray) {
                marker.getElement().classList.add('label-hidden');
            }
        }else {
            for (let marker of markersArray) {
                if(this.listOfPermanentTooltipBuildings.includes(marker.options.alt)) {
                    marker.getElement().classList.remove('label-hidden');
                }else {
                    marker.getElement().classList.add('label-hidden');
                }
            }
        }
    }

    drawMarkers(quarter) {

        //remove all previous markers before drawing new ones
        if(this.markersArray) {
            for(let marker of this.markersArray){
                this.mapService.map.removeLayer(marker);
            }
        }

        this.markersArray = [],
        this.popupsArray = [],
        this.currentZoom = this.mapService.map.getZoom();

        this.getMarkerData(quarter).subscribe(
            data => {
                let facLinkData = data[0];
                let thermooData = data[1];

                let combinedData = [];

                let capitalizedQuarter = quarter.charAt(0).toUpperCase() + quarter.slice(1);
                let readableQuarter = capitalizedQuarter.replace(/(\d+)/g, function (_, num){
                    return ' ' + num;
                });

                let statistics = this.findStatistics(thermooData);

                //TODO: separate out nested for loops for clarity of code
                for (let thermooValue of thermooData) {

                    for (let facLinkValue of facLinkData) {

                        if(facLinkValue.key === thermooValue.buildingKey) {
                            if (!facLinkValue.longitude || !facLinkValue.latitude) {
                                console.log("Missing lat/long for "+facLinkValue.name);
                                break;
                            } else {

                                let className = this.findMarkerClass(thermooValue.comfortSummary);

                                combinedData.push({
                                    comfortSummary: thermooValue.comfortSummary,
                                    longitude: facLinkValue.longitude,
                                    latitude: facLinkValue.latitude,
                                    name: thermooValue.buildingName,
                                    className: className,
                                    sum: statistics.allSums[thermooValue.buildingName],
                                    max: statistics.allMaxes[thermooValue.buildingName]
                                });

                                break;
                            }
                        }
                    }
                }
			this.buildMarker(combinedData, readableQuarter, statistics);
		},
            err => console.error(err)
        );
	}
	
	buildMarker(build, qrtr, stats){
		let biggestMarkerSize = 40,
			smallestMarkerSize = 13;
		let sizeRange = biggestMarkerSize - smallestMarkerSize;

		for (let markerDataValue of build) {

			let markerHeight = 0;
			
			//Set Rifle Range to smallest marker size
			if(markerDataValue.name === "Rifle Range") {
			  markerHeight = smallestMarkerSize;
			}else {
			  markerHeight = Math.round((((markerDataValue.sum - stats.min) / stats.range) * sizeRange) + smallestMarkerSize);
			}

			let latlong = L.latLng(markerDataValue.latitude, markerDataValue.longitude),
			markerWidth = Math.round(markerHeight * 0.75),
			percentMaxVote = Math.round((markerDataValue.max / markerDataValue.sum)*100);

			//define marker SVG with recalculated width/height
			let iconSvg = this.mapPopup.getIcon(markerHeight, markerWidth),
			labelDiv = this.mapPopup.getLabel(markerDataValue),
			tooltipDiv = this.mapPopup.getTooltip(markerDataValue, percentMaxVote);

			//define popup
			let popup = L.popup({
				offset: [0, (4-markerHeight)],
				minWidth: 328,
				maxWidth: 328
			}).setContent(this.mapPopup.getDefaultPopup(markerDataValue, percentMaxVote, qrtr));

			// define marker icon
			let markerIcon = L.divIcon({
				html: iconSvg + labelDiv,
				iconSize: [markerWidth, markerHeight],
				iconAnchor: [Math.round(markerWidth*0.5), markerHeight],
				className: markerDataValue.className
			});

			// define marker object with marker icon
			let marker = L.marker(latlong, {
				icon: markerIcon,
				alt: markerDataValue.name,
				riseOnHover: true,
			})
			.addTo(this.mapService.map)

			.bindTooltip(tooltipDiv,
			{
				direction: 'right',
				offset: [10,-50],
				sticky: true
			})

			.bindPopup(popup);


			//open the tooltip without mouse interaction if certain conditions are met
			if(!this.listOfPermanentTooltipBuildings.includes(markerDataValue.name) && this.currentZoom > 15 && this.currentZoom < 18) {
				marker.getElement().classList.add('label-hidden');
			}

			marker.on("mouseover", (e: MouseEvent) => {
				if(this.currentZoom < 18) {
					marker.getElement().classList.remove('label-hidden');
				}
			});
			marker.on("mouseout", (e: MouseEvent) => {
				if((!this.listOfPermanentTooltipBuildings.includes(markerDataValue.name) && this.currentZoom > 15 && this.currentZoom < 18) || this.currentZoom <= 15) {
					marker.getElement().classList.add('label-hidden');
				}
			});

			//unbind tooltip when popup is opened on marker
			//and store tooltip in a temporary variable
			let tooltipHoldingTank = undefined;
			marker.on("popupopen", (e: MouseEvent) => {
				tooltipHoldingTank = marker.getTooltip();
				marker.unbindTooltip();
			});
			//rebind tooltip when popup is closed
			marker.on("popupclose", (e: MouseEvent) => {
				marker.bindTooltip(tooltipHoldingTank);
				tooltipHoldingTank = undefined;
			});

			//array to track markers for later interactions
			this.markersArray.push(marker);

			//array to track popups
			this.popupsArray.push(popup);
		}
        this.mapService.map.on("zoom", (e: MouseEvent) => {
            this.resetMapLabels(e,this.markersArray);
        });
        this.mapService.map.on("click", (e: MouseEvent) => {
            this.resetMapLabels(e,this.markersArray);
        });	
	}
}
